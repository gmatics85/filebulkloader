﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileBulkLoader
{
    public class GdsDecisionsTemp
    {
        public int DEC_CUS_AccountingWeek { get; set; }
        public Int64 DEC_CUS_Customer_Id { get; set; }
        public string DEC_CUS_DecisionLeafCode { get; set; }
        public string DEC_CUS_DecisionLeafDesc { get; set; }
        public string DEC_CUS_EndPointProcess { get; set; }
        public string DEC_CUS_CollectionScore { get; set; }
        public string DEC_AGR1_LT_LetterType { get; set; }
        public string DEC_AGR2_LT_LetterType { get; set; }
        public string DEC_AGR3_LT_LetterType { get; set; }
        public string DEC_AGR4_LT_LetterType { get; set; }
        public string DEC_AGR5_LT_LetterType { get; set; }
        public string DEC_AGR6_LT_LetterType { get; set; }
        public string DEC_SMS_SMSType { get; set; }
        public string DEC_CPP_PHReminderAgreements { get; set; }
        public string DEC_CPP_PFReminderAgreements { get; set; }
        public string DEC_CPP_PHDMReminderAgreements { get; set; }
        public string DEC_CPP_PFDMReminderAgreements { get; set; }
        public string DEC_Campaign_Id { get; set; }
        public string DEC_ADP_AdeptraType { get; set; }
        public string DEC_CUS_ActionDate { get; set; }
        public string DEC_CDT_SkipTraceId { get; set; }
        public string DEC_CDT_SkipTraceStatus { get; set; }
        public string DEC_PTP_PromiseStatus { get; set; }
        public string DEC_PTP_PromiseID { get; set; }
        public string DEC_CUS_Strategy01 { get; set; }
        public string DEC_CUS_Strategy02 { get; set; }
        public string DEC_CUS_Strategy03 { get; set; }
        public string DEC_CUS_Strategy04 { get; set; }
        public string DEC_CUS_Strategy05 { get; set; }
        public string DEC_CUS_ChampChall01 { get; set; }
        public string DEC_CUS_ChampChall02 { get; set; }
        public string DEC_CUS_ChampChall03 { get; set; }
        public string DEC_CUS_ChampChall04 { get; set; }
        public string DEC_CUS_ChampChall05 { get; set; }
        public string DEC_PHO_IsFlagWRA { get; set; }
        public string DEC_PHO_VerCampOrder { get; set; }
        public string DEC_FLD_ActionDate { get; set; }
        public string DEC_FLD_ActionType { get; set; }
        public string DEC_FLD_EndPointProcess { get; set; }
        public string DEC_FLD_DecisionLeafCode { get; set; }
        public string DEC_FLD_DecisionLeafDesc { get; set; }

        public string DEC_Agr_1_Agreement_Num { get; set; }
        public string DEC_Agr_2_Agreement_Num { get; set; }
        public string DEC_Agr_3_Agreement_Num { get; set; }
        public string DEC_Agr_4_Agreement_Num { get; set; }
        public string DEC_Agr_5_Agreement_Num { get; set; }
        public string DEC_Agr_6_Agreement_Num { get; set; }
    }
}
