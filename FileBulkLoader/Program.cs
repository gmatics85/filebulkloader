using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ChoETL;

namespace FileBulkLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            const string filePath = "D:\\Work\\gds-file-190327132049.BatchDE.DAT";
            var gdsDecTemp = new List<GdsDecisionsTemp>();

            using (var reader = new ChoCSVReader<GdsDecisionsTemp>(filePath)
                                        .WithDelimiter("\t")
                                        .WithEOLDelimiter("\t\r")
                                        .WithFirstLineHeader())
            {
                gdsDecTemp = (from lines in reader select lines).ToList();
            }

            var objBulk = new BulkUploadToSql<GdsDecisionsTemp>()
            {
                InternalStore = gdsDecTemp,
                TableName = typeof(GdsDecisionsTemp).Name,
                CommitBatchSize = 1000,
                ConnectionString = "Data Source=(LocalDb)\\MSSQLLocalDB;Database=COLL_ROM_Main;Integrated Security=True;"
            };
            objBulk.Commit();
        }
    }
}
